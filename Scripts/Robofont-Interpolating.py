# import AppKit
import ezui
import math
import merz


from fontTools.pens.transformPen import TransformPen
from fontTools.misc.transform import Transform
from defcon import Glyph, Font, Kerning, Info, registerRepresentationFactory
from mojo.extensions import getExtensionDefault, setExtensionDefault, removeExtensionDefault
from mojo.subscriber import Subscriber, registerRoboFontSubscriber
from mojo.UI import MenuBuilder, splitText
from merz.tools.drawingTools import NSImageDrawingTools

from mojo.tools import profileDecorator, timerDecorator, IntersectGlyphWithLine



sliderWidth = 690
leftTextWidth = 75
sideOffset = 10
bottomMargin = 200
boxHeight = 310


firstSource = AllFonts()[0]
secondSource = (
    AllFonts()[1] 
    if len(AllFonts()) > 1
    else firstSource
)     


ufostretchDefaultKey = "com.typemytype.ufostretch"

defaultParameters = {'leftLetters': 'na', 'middleLetters': 'A', 'rightLetters': 'nn', 'shiftXSlider': 0, 'shiftYSlider': 0, 'skewSlider': 0, 'trackingSlider': 0, 'scaleXSlider': 100, 'scaleCheckbox': 0, 'scaleYSlider': 100, 'interpolXSlider': 50, 'interpolCheckbox': 1, 'interpolYSlider': 50}
defaultDisplaySettings = {"fillOption":1, "strokeOption" : 0, "onCurveOption" : 0, "offCurveOption" : 0, 'bothSources':0}
defaultDisplayColors = {"glyph fill":(0,0,0, 1), "glyph stroke":(0,0,0, 1), "glyph points":(1,.4,.7, 1)}


def beamXSliderImageFactory(color=(1, 0, 0, 1)):
    r, g, b, a = color
    size = 20, 40
    width, height = size
    bot = NSImageDrawingTools(size)
    bot.fill(r, g, b, a)
    bot.roundedRect(10, 0, 10, 40, 5)
    image = bot.getImage()
    return image

def stretchGlyphFactory(glyph, secondSource, **parameters):
    if glyph.name not in secondSource:
        None

    secondGlyph = secondSource[glyph.name]
    glyph = glyph.asFontParts()
    secondGlyph = secondGlyph.asFontParts()
    
    interpol_x = parameters["interpolXSlider"]/100
    interpol_y = parameters["interpolXSlider"]/100

    # list index out of range Error happens here
    interpolationResult = glyph + (secondGlyph - glyph) * (interpol_x, interpol_y)
    
    result = RGlyph()
    transform = Transform(1,0,0,1,0,0)
    transformPen = TransformPen(
        result.getPen(),
        transform
    )
    interpolationResult.draw(transformPen)
    
    result.name = glyph.name
    result.width = transform.transformPoint((interpolationResult.width, 0))[0]
    
    return result
    



class UFOstretch(Subscriber, ezui.WindowController):
    
    def build(self):
        self.scrollingView = None
        content = """
        *VerticalStack
        > *HorizontalStack
        >> [_Hello_]                    @middleLetters
        >> (...)                        @displayPullDownButton
        
        > * ScrollingMerzView           @scrollingView

        > * VerticalStack               @sliderStack    
        >> *HorizontalStack
        >>> Interpolate:                  @interpolText
        >>> ---X--- [_](±)               @interpolXSlider
        """
        parameters = (
            self.firstSource.lib[f"{ufostretchDefaultKey}.parameters"]
            if f"{ufostretchDefaultKey}.parameters" in self.firstSource.lib
            else defaultParameters  
        )
        removeExtensionDefault(f"{ufostretchDefaultKey}.displaySettings")
        displaySettings = getExtensionDefault(f"{ufostretchDefaultKey}.displaySettings", fallback=defaultDisplaySettings)
        displayColors = getExtensionDefault(f"{ufostretchDefaultKey}.displayColors", fallback=defaultDisplayColors)
        
        
        descriptionData = dict(
            middleLetters=dict(
                width= "fill",
                value=parameters['middleLetters'],
            ),
            displayPullDownButton=dict(
                itemDescriptions=[
                    dict(
                        identifier="fillOption",
                        text="Fill",
                        state=displaySettings["fillOption"],
                    ),
                    dict(
                        identifier="strokeOption",
                        text="Stroke",
                        state=displaySettings["strokeOption"],
                    ),
                    dict(
                        identifier="onCurveOption",
                        text="On-curve Points",
                        state=displaySettings["onCurveOption"],
                    ),
                    dict(
                        identifier="offCurveOption",
                        text="Off-curve Points",
                        state=displaySettings["offCurveOption"],
                    ),
                    dict(
                        identifier="bothSources",
                        text="Show Original",
                        state=0,
                    ),
                ],
                # alignment="center"
            ),            
            scrollingView=dict(
                delegate=self,
                centered=False,
            ),
            interpolText=dict(
                width=leftTextWidth,
            ),
            interpolXSlider=dict(
                value=parameters['interpolXSlider'],
                minValue=-100,
                maxValue=200,
            ),
        )
        self.w = ezui.EZWindow(
            title="Interpolate",
            content=content,
            descriptionData=descriptionData,
            controller=self,
            margins= 10,
            size=(850,400),
            minSize = (500,300),
            activeItem="middleLetters"
        )
        
        self.scrollView = self.w.getItem("scrollingView")
        self.container = self.scrollView.getMerzContainer()
        
        self.UFOstretchLayer = self.container.appendBaseSublayer(
            name="toplayer",
            position=(dict(point="left", offset=sideOffset), "bottom"),
            size=("right", "top"),
        )
        self.scaledContainer = self.UFOstretchLayer.appendBaseSublayer(
            position=("left", "bottom"),
            size=("right", "top"),
            horizontalAlignment="center",
        )
        self.letterLineLayer = self.scaledContainer.appendBaseSublayer(
            position=("left", "bottom"),
            size=("right", "top"),
        )       
        self.middleLetterLayer = self.letterLineLayer.appendBaseSublayer(
            name="middleLetterLayer",
            position=("left", "bottom"),
            size=("right", "top"),
        )
    

    firstSource = AllFonts()[0]
    secondSource = (
        AllFonts()[1] 
        if len(AllFonts()) > 1
        else firstSource
    )     
    angle = math.pi / 2
    ufoStretchScale = 1
    ufoStretchOffset = 20
    

    def started(self):
        self.w.open()        
        self.drawLetters()
        self.setAdjunctGlyphs()
                
    def destroy(self):
        parameters = self.w.getItemValues()
        self.firstSource.lib[f"{ufostretchDefaultKey}.parameters"] = parameters
        setExtensionDefault(f"{ufostretchDefaultKey}.displaySettings", self.getDisplaySettings())
        
    def middleLettersCallback(self, sender):
        self.drawLetters(["middle"])

    def interpolXSliderCallback(self,sender):
        self.drawLetters(["middle"])   
            
    def fillOptionCallback(self,sender):
        displayPullDown = self.w.getItem("displayPullDownButton")
        displayPullDown.setMenuItemState("fillOption", not sender.state())
        
        fillOption = displayPullDown.getMenuItemState("fillOption")        
        strokeOption = displayPullDown.getMenuItemState("strokeOption")

        for container in self.getUFOStretchGlyphContainers():
            container.enableFillAndStroke(fillOption,strokeOption)

    def strokeOptionCallback(self,sender):
        displayPullDown = self.w.getItem("displayPullDownButton")
        displayPullDown.setMenuItemState("strokeOption", not sender.state())
        
        fillOption = displayPullDown.getMenuItemState("fillOption")        
        strokeOption = displayPullDown.getMenuItemState("strokeOption")

        for container in self.getUFOStretchGlyphContainers():
            container.enableFillAndStroke(fillOption,strokeOption)

    def onCurveOptionCallback(self,sender):
        displayPullDown = self.w.getItem("displayPullDownButton")
        displayPullDown.setMenuItemState("onCurveOption", not sender.state())
        state = sender.state()
        
        for container in self.getUFOStretchGlyphContainers():
            container.enableOnCurvePoints(state)

    def offCurveOptionCallback(self,sender):
        displayPullDown = self.w.getItem("displayPullDownButton")
        displayPullDown.setMenuItemState("offCurveOption", not sender.state())
        state = sender.state()

        for container in self.getUFOStretchGlyphContainers():
            container.enableOffCurvePoints(state)

    def bothSourcesCallback(self,sender):
        displayPullDown = self.w.getItem("displayPullDownButton")
        displayPullDown.setMenuItemState("bothSources", not sender.state())
        state = sender.state()

        for container in self.getUFOStretchGlyphContainers():
            container.enableBothSources(state)
                    
        
    # ---- FUNCTIONS
    
    def drawLetters(self, positions=["middle"]):
        positionY = -self.firstSource.info.descender+bottomMargin
        for position in positions:
            letterLayer = self.letterLineLayer.getSublayer(f"{position}LetterLayer")
            letterLayer.clearSublayers()
            
            for glyph in self.getGlyphs([position]):                
                UFOstretchGlyph = letterLayer.appendSublayerOfClass(
                    UFOStretchGlyphContainer,
                    name=f"{glyph.name}.{glyph.font}",
                    position = (0, 'bottom'),
                    size=(glyph.width+500, 'top'),
                    glyph=glyph,
                    positionY = positionY,
                    displaySettings = self.getDisplaySettings(),
                )
        self.setContainerPositions()
        self.setViewSize()
        self.setAdjunctGlyphs()
        

    def getGlyphs(self, positions=["middle"]):
        parameters = self.w.getItemValues()
        charSet = self.firstSource.asDefcon().unicodeData
        displayPullDownButton = self.w.getItem("displayPullDownButton")
        
        letters = []
        
        for position in positions:
            typedLetters = self.w.getItemValue(f"{position}Letters")
            for letter in splitText(typedLetters, charSet):
                glyph = None
                                                    
                if letter in self.firstSource:
                    glyph = self.firstSource[letter]
                if position == 'middle':
                    try:
                        glyph = self.firstSource[letter].getRepresentation("stretchGlyph", secondSource=self.secondSource, **parameters)
                    except Exception as e:
                        print(e)
                        glyph = Glyph()
                        glyph.name = f'{letter}.UFOstretch.InterpolationError'
                        glyph.width = 500
                    
                if glyph is not None:
                    letters.append(glyph)
     
        return letters
        
         
    def getDisplaySettings(self):
        displayPullDownButton = self.w.getItem("displayPullDownButton")
        newDisplaySettings = {}
                
        for item in defaultDisplaySettings:
            displayItem = displayPullDownButton.getMenuItem(item)
            newDisplaySettings[item] = displayItem.state()
                
        return newDisplaySettings

    def getUFOStretchGlyphContainers(self):
        UFOStretchGlyphContainerList = []
            
        for layer in self.middleLetterLayer.getSublayers():
            UFOStretchGlyphContainerList.append(layer)
        
        return UFOStretchGlyphContainerList        

    def setAdjunctGlyphs(self):
        glyphs = set()
        for glyph in self.getGlyphs():
            glyphName = glyph.name
            if glyph.font is not None:
                if glyph.name in self.firstSource:
                    glyphs.add(self.firstSource[glyph.name])
                if self.secondSource is not None and glyph.name in self.secondSource:
                    glyphs.add(self.secondSource[glyph.name])
            else:
                if "UFOstretch.InterpolationError" in glyph.name:
                    glyphName = glyph.name[:-30]
                if glyphName in self.firstSource:
                    glyphs.add(self.firstSource[glyphName])
                if glyphName in self.secondSource:
                    glyphs.add(self.secondSource[glyphName])
        
        self.setAdjunctObjectsToObserve(glyphs)
        
    def setContainerPositions(self):
        posX = 0
        for i, glyph in enumerate(self.getGlyphs()):
            container = self.getUFOStretchGlyphContainers()[i]
            container.setPosition((posX, "bottom"))
            posX += glyph.width
            
    def setViewSize(self):
        w, h = self.scrollView.getMerzViewSize()
        w = sideOffset + 1
        for glyph in self.getGlyphs():
            w += glyph.width * self.ufoStretchScale
        
        self.scrollView.setMerzViewSize((w,h))
            
        
        
    ##___ EVENTS

    def acceptsFirstResponder(self, sender):
        return True  
    
    def scrollViewSizeChanged(self, sender, size):
        self.windowSize = size
        
        w, h = size
        self.ufoStretchScale = h/(self.firstSource.info.unitsPerEm + bottomMargin + 250)
        # calculate the line width
        w = sideOffset + 1
        for glyph in self.getGlyphs():
            w += glyph.width * self.ufoStretchScale
        sender.setMerzViewSize((w, h))
    
        self.scaledContainer.addScaleTransformation(self.ufoStretchScale, name="scale")

    def adjunctGlyphDidChange(self, info):
        if info['glyph'].font == self.secondSource:
            firstLetter = self.firstSource[info['glyph'].name]
            firstLetter.asDefcon().destroyRepresentation("stretchGlyph")

        self.drawLetters()




class UFOStretchGlyphContainer(merz.Container):
    
    def __init__(self, glyph, *args, name=None, positionY=0, beamXPos=0, beamYPos=0, beamYAngle=0, fontInfo=None, displaySettings=None, **kwargs):
        self.ufoStretchGlyph = None
        
        super().__init__(*args, **kwargs)
        
        #removeExtensionDefault(f"{ufostretchDefaultKey}.displayColors")
        self.displayColors = getExtensionDefault(f"{ufostretchDefaultKey}.displayColors", fallback=defaultDisplayColors)
            
        self.name = name
        self.displaySettings = displaySettings
        self.positionY = positionY
        
        self.bothSourcesLayer = self.appendPathSublayer(
            position = (0, self.positionY),
            strokeWidth = 1,
            #strokeColor = (1,0,0,.5),
            fillColor = (0,0,0, .3),
        )
            
        self.ufoStretchGlyphPath = self.appendPathSublayer(position = (0, self.positionY))
        
        self.onCurveLayer = self.appendBaseSublayer(position = (0, self.positionY))
        self.offCurveLayer = self.appendBaseSublayer(position = (0, self.positionY))
                
        self.buildUFOStretchGlyph(glyph)
        
        
        
    def buildUFOStretchGlyph(self, glyph):
        """
        bouw alle layers
        """        
        self.ufoStretchGlyph = glyph
        
                        
        if 'UFOstretch.InterpolationError' in self.ufoStretchGlyph.name:
            self.appendTextLineSublayer(
               position=(250,self.positionY + 250),
               anchor=(.5,.5),
               horizontalAlignment = 'center',
               verticalAlignment = 'center',
               text="¯\\_(ツ)_/¯\n oops can't \n interpolate",
               pointSize=8,
               fillColor=(1, 0, 0, 1),
            )
            
        else:
            self.ufoStretchGlyphPath.setPath(self.ufoStretchGlyph.getRepresentation("merz.CGPath"))
        
            self.drawOnCurvePoints(glyph)
            self.drawOffCurvePoints(glyph)
            self.drawBothSources()
        
            self.enableFillAndStroke(self.displaySettings["fillOption"], self.displaySettings["strokeOption"])
            self.enableOnCurvePoints(self.displaySettings["onCurveOption"])
            self.enableOffCurvePoints(self.displaySettings["offCurveOption"])
            self.enableBothSources(self.displaySettings["bothSources"])
            
        
    def drawOnCurvePoints(self, glyph):
        self.onCurveLayer.clearSublayers()
        
        for contour in glyph:
            for point in contour.points:
                if not point.type == "offcurve":
                    self.onCurveLayer.appendSymbolSublayer(
                        position=(point.x, point.y),
                        offset=(0,0),
                        imageSettings=dict( 
                            name="rectangle",
                            size = (3.5,3.5),
                            fillColor = self.displayColors["glyph points"],
                        )
                    )

    def drawOffCurvePoints(self, glyph):
        self.offCurveLayer.clearSublayers()
    
        for contour in glyph:
            for i, point in enumerate(contour.points):
                if point.type == "offcurve":
                    self.offCurveLayer.appendSymbolSublayer(
                        position=(point.x, point.y),
                        offset=(0,0),
                        imageSettings=dict( 
                            name="oval",
                            size = (3.5,3.5),
                            fillColor = self.displayColors["glyph points"],
                        )
                    )
                    if not contour.points[i-1].type == "offcurve":
                        self.offCurveLayer.appendLineSublayer(
                           startPoint=(point.x, point.y),
                           endPoint=(contour.points[i-1].x, contour.points[i-1].y),
                           strokeWidth=.6,
                           strokeColor=self.displayColors["glyph points"],
                        )
                    nextPointIndex = (i+1) % len(contour.points)
                    if not contour.points[nextPointIndex].type == "offcurve":
                        self.offCurveLayer.appendLineSublayer(
                           startPoint=(point.x, point.y),
                           endPoint=(contour.points[nextPointIndex].x, contour.points[nextPointIndex].y),
                           strokeWidth=.6,
                           strokeColor=self.displayColors["glyph points"],
                        )
    
    def drawBothSources(self):
        self.bothSourcesLayer.clearSublayers()
        
        firstSourceGlyph = firstSource[self.ufoStretchGlyph.name]
        
        self.bothSourcesLayer.setPath(firstSourceGlyph.getRepresentation("merz.CGPath"))
        
        originalPointsLayer = self.bothSourcesLayer.appendBaseSublayer()
       
        for contour in firstSourceGlyph:
            for point in contour.points:
                if not point.type == "offcurve":
                    originalPointsLayer.appendSymbolSublayer(
                        position=(point.x, point.y),
                        offset=(0,0),
                        imageSettings=dict( 
                            name="rectangle",
                            size = (3.5,3.5),
                            fillColor = (0,0,1,1),
                        )
                    )
        
        for i, firstPointLayer in enumerate(self.onCurveLayer.getSublayers()):
            secondPointLayer = originalPointsLayer.getSublayers()[i]
            firstPosition = firstPointLayer.getPosition()
            secondPosition = secondPointLayer.getPosition()

            self.bothSourcesLayer.appendLineSublayer(
               startPoint=firstPosition,
               endPoint=secondPosition,
               strokeWidth=.6,
               strokeColor=(0,0,1,1),
            )


            
        

    def enableFillAndStroke(self, fillValue, strokeValue):
        if fillValue:
            if strokeValue:
                with self.ufoStretchGlyphPath.propertyGroup():
                    self.ufoStretchGlyphPath.setStrokeWidth(.7)
                    self.ufoStretchGlyphPath.setStrokeColor(self.displayColors["glyph stroke"])
                    
                    r, g, b, a = self.displayColors["glyph fill"]
                    self.ufoStretchGlyphPath.setFillColor((r, g, b, a/2))
            else:
                self.ufoStretchGlyphPath.setFillColor(self.displayColors["glyph fill"])
                self.ufoStretchGlyphPath.setStrokeColor(None)
        else:
            if strokeValue:
                with self.ufoStretchGlyphPath.propertyGroup():
                    self.ufoStretchGlyphPath.setStrokeWidth(.7)
                    self.ufoStretchGlyphPath.setStrokeColor(self.displayColors["glyph stroke"])
                    self.ufoStretchGlyphPath.setFillColor(None)
            else:
                self.ufoStretchGlyphPath.setStrokeColor(None)
                self.ufoStretchGlyphPath.setFillColor(None)

    def enableOnCurvePoints(self, value):
        self.onCurveLayer.setVisible(value)

    def enableOffCurvePoints(self, value):
        self.offCurveLayer.setVisible(value)
        
    def enableBothSources(self, value):
        self.bothSourcesLayer.setVisible(value)


registerRepresentationFactory(Glyph, "stretchGlyph", stretchGlyphFactory)
  
UFOstretch()