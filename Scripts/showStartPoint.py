import ezui
import merz
from mojo.subscriber import Subscriber
from mojo.subscriber import registerGlyphEditorSubscriber, unregisterGlyphEditorSubscriber


class drawReferenceGlyph(Subscriber):
    
    debug = True
    
    def build(self):
        glyphEditor = self.getGlyphEditor()
        self.container = glyphEditor.extensionContainer(
            identifier="com.roboFont.DrawReferenceLayer.foreground",
            location="foreground",
            clear=True
        )
        
        glyph = CurrentGlyph()        
        
        for i, contour in enumerate(glyph.contours):
            startPosition = contour.points[0].position
                        
            self.container.appendSymbolSublayer(
            position=startPosition,
            imageSettings=dict(
                name="oval",
                size=(7, 7),
                fillColor=(1, 1, 0, 1)
                )
            )
            self.container.appendTextLineSublayer(
                text = f"Contour {i+1}",
                position = startPosition,
                pointSize = 12,
                backgroundColor = (1,1,0, .3),
            )
                
    
    def destroy(self):
        self.container.clearSublayers()
        
    def glyphDidChange(self, info):
        self.container.clearSublayers()
        
        glyph = CurrentGlyph()        
        
        for i, contour in enumerate(glyph.contours):
            startPosition = contour.points[0].position
                        
            self.container.appendSymbolSublayer(
            position=startPosition,
            imageSettings=dict(
                name="oval",
                size=(7, 7),
                fillColor=(1, 1, 0, 1)
                )
            )
            self.container.appendTextLineSublayer(
                text = f"Contour {i+1}",
                position = startPosition,
                pointSize = 12,
                backgroundColor = (1,1,0, .3),
            )

class DemoController(ezui.WindowController):

    font = None

    def build(self):
        content = """
        (Show Startpoints) @showStartPointsButton
        """
        self.w = ezui.EZWindow(
            title="Show Startpoint",
            size=(150,50),
            content=content,
            controller=self
        )

    def started(self):
        self.w.open()
    
    def destroy(self):
        unregisterGlyphEditorSubscriber(drawReferenceGlyph)
        drawReferenceGlyph.controller = None
        
    def showStartPointsButtonCallback(self, sender):
        drawReferenceGlyph.controller = self
        registerGlyphEditorSubscriber(drawReferenceGlyph)        

        
DemoController()