canvasWidth = 400
canvasHeight = 300

leftMargin = 50
bottomMargin = 120

#make sure the min & max weight match the values of your variable font
minWeight = 0
maxWeight = 1000

numFrames = 36

word = "Money"
fontSource = '/Users/Marte/Documents/TYPE/_WORK/202401_KABKworkshop/ExampleVariable/Variable/Money-Money-VF.ttf'


for frame in range(numFrames):
    newPage(canvasWidth, canvasHeight)
    
    fill(1,1,1, 1) # background color (r,g,b, a)
    rect(0,0, 400, 300)
    
    font(fontSource)
    fontSize(100) # Change font size here

    frameDuration(1/20) # How fast do you want your animation to be? (in seconds)
    
    # rotate(frame*10, center=(canvasWidth/2,canvasHeight/2)) # Rotate font here
    
    t = (frame*180)/numFrames
    factor = sin(radians(t))
    
    w = minWeight + factor * (maxWeight - minWeight)
    fontVariations(wght=w)
    
    fill(1,0,0, 1)
    text(word, (leftMargin, bottomMargin))    

        
saveImage("interpol.gif", imageResolution=300)