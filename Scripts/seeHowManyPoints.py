font1 = AllFonts()[0]
font2 = AllFonts()[1]

# Make sure you have your glyph selected/open

glyph = CurrentGlyph()
otherGlyph = font2[glyph.name]

print(f"First {glyph.name}:")
for i, contour in enumerate(glyph):
    glyphPoints = 0
    for point in contour.points:
        if point.type != "offcurve":
            glyphPoints += 1
    print(f"- Contour {i+1}: {glyphPoints} points")
    
print()
print(f"Second {otherGlyph.name}:")
for i, contour in enumerate(otherGlyph):
    glyphPoints = 0
    for point in contour.points:
        if point.type != "offcurve":
            glyphPoints += 1
    print(f"- Contour {i+1}: {glyphPoints} points")