# Animated Typography Workshop

## RoboFont Beta Version
* [https://pointer.click/files/typeactivismfiles.zip](https://pointer.click/files/typeactivismfiles.zip)

## Font Sources
* [https://fonts.google.com](https://fonts.google.com)
* [https://fontsource.org](https://fontsource.org - google fonts + more, without tracking)
* [https://velvetyne.fr](https://velvetyne.fr)
* [https://www.collletttivo.it](https://www.collletttivo.it)
* [https://fontlibrary.org](https://fontlibrary.org)
* [https://www.theleagueofmoveabletype.com](https://www.theleagueofmoveabletype.com)
* [https://v-fonts.com/licenses/open-source](https://v-fonts.com/licenses/open-source)

## Words
* Money
* Nation
* Power
* Action
* Love
* Protest
* History
* People
* Change
* Conflict
* Spring
* Momentum
* Memory
* World
* Dawn
* News
* Culture
* Crown
* Fire
* Border
* Pressure
* Politics
* Clash
* Future
* Roots
* Identity
* Information

## Variable Font Inspirations
* [https://typemedia.org/tm2021/responsive/](https://typemedia.org/tm2021/responsive/)
* [https://typemedia.org/tm2122/responsive/](https://typemedia.org/tm2122/responsive/)

## Documentation & Links
* Robofont: [https://robofont.com](https://robofont.com)
* Drawbot: [https://www.drawbot.com](https://www.drawbot.com)
* Fontgauntlet: [https://fontgauntlet.com](https://fontgauntlet.com)
* VariableTime: [https://variabletime.pointer.click](https://variabletime.pointer.click)
* Samsa: [https://lorp.github.io/samsa/src/samsa-gui.html](https://lorp.github.io/samsa/src/samsa-gui.html)

![Sample video](RobofontVariables101.mp4)

## Projection
![Projection Mask](projection_mask_left.png)
[Projection Test Program (Mac)](https://pointer.click/files/chapel_kabk.zip)

## Online Gallery
[online gallery](https://kabk.pointer.click/2024/workshops/animatedtype/)
[source code](https://gitlab.com/pointerstudio/kabk/godot/chapel_projection/)
